﻿using Aforo255.Cross.Metric.Registry;
using Microsoft.AspNetCore.Mvc;
using MS.AFORO255.Account.Service;

namespace MS.AFORO255.Account.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AccountController : ControllerBase
{
    private readonly IAccountService _accountService; 
    private readonly IMetricsRegistry _metricsRegistry;

    public AccountController(IAccountService accountService, IMetricsRegistry metricsRegistry)
    {
        _accountService = accountService;

        _metricsRegistry = metricsRegistry;
    }

    [HttpGet]
    public IActionResult Get()
    {
        _metricsRegistry.IncrementFindQuery();
        return Ok(_accountService.GetAll());
    }
}
